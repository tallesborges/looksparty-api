var uuid = require('node-uuid');

module.exports = function (app, cb) {
  /*
   * The `app` object provides access to a variety of LoopBack resources such as
   * models (e.g. `app.models.YourModelName`) or data sources (e.g.
   * `app.datasources.YourDataSource`). See
   * http://docs.strongloop.com/display/public/LB/Working+with+LoopBack+objects
   * for more info.
   */
  app.datasources.localUploads.connector.getFilename = function (file, req, res) {
    var origFilename = file.name;
    var parts = origFilename.split('.');
    var extension = parts[parts.length - 1];
    return uuid.v1() + '.' + extension;
  };
  process.nextTick(cb); // Remove if you pass `cb` to an async function yourself
};
