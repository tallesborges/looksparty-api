module.exports = function (Container) {

  Container.afterRemote('upload', function (ctx, modelInstance, next) {
    var files = ctx.result.result.files;

    Object.keys(files).forEach(function (key) {
      var file = files[key][0];
      file.url = "/containers/" + file.container + "/download/" + file.name;
      ctx.result = file;
      next();
    });

  });

};
