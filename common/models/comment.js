module.exports = function(Comment) {

  Comment.observe('before save', function (ctx, next) {

    if (ctx.instance) {
      ctx.instance.date_created = new Date();
      next();
    }

  });

};
