module.exports = function(Publication) {

  Publication.observe('before save', function (ctx, next) {

    if (ctx.instance) {
      ctx.instance.date_created = new Date();
      next();
    }

  });

};
