angular
  .module('app', [
    'lbServices',
    'ui.router',
    'angularFileUpload'
  ])
  .config(['$stateProvider', '$urlRouterProvider', function($stateProvider,
                                                            $urlRouterProvider) {
    $stateProvider
      .state('mask', {
        url: '',
        templateUrl: 'views/mask.html',
        controller: 'MaskController'
      });

    $urlRouterProvider.otherwise('mask');
  }]);
