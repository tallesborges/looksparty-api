angular.module('app')
  .controller('MaskController', ['$scope', '$state', 'Mask', 'FileUploader', function ($scope,
                                                                                       $state, Mask, FileUploader) {
    'use strict';

    $scope.masks = [];
    $scope.urlUploadItem = 'not uploaded';

    var uploader = $scope.uploader = new FileUploader({
      scope: $scope,
      autoUpload: false,
      removeAfterUpload: true,
      url: '/api/containers/mask_images/upload',
      formData: [
        {key: 'value'}
      ]
    });

    uploader.onSuccessItem = function (item, response, status, headers) {
      console.info('Success', response, status, headers);
      $scope.urlUploadItem = response.url;
    };

    function getMasks() {
      Mask
        .find()
        .$promise
        .then(function (results) {
          $scope.masks = results;
        });
    }

    getMasks();

    $scope.addMask = function () {
      Mask
        .create($scope.newMask)
        .$promise
        .then(function (mask) {
          $scope.newMask = '';
          $scope.maskForm.thumbUrl.$setPristine();
          $scope.maskForm.maskUrl.$setPristine();
          $('.focus').focus();
          getMasks();
        });
    };

    $scope.updateMask = function (item) {
      Mask
        .updateOrCreate(item)
        .$promise
        .then(function (mask) {
          $scope.newMask = '';
          $scope.maskForm.thumbUrl.$setPristine();
          $scope.maskForm.maskUrl.$setPristine();
          $('.focus').focus();
          getMasks();
        });
    };

    $scope.removeMask = function (item) {
      Mask
        .deleteById(item)
        .$promise
        .then(function () {
          getMasks();
        });
    };
  }]);
